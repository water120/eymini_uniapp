/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2020-1-1
 */
import setting from './setting.js';
let tpl = 'ApiCms'; // 小程序模板ID

const accountInfo = uni.getAccountInfoSync();
let appId = accountInfo.miniProgram.appId; // 小程序appid
let domain = setting.domain; // 网站域名
let apikey = setting.apikey; // 开源小程序API接口密钥    

let root_dir = setting.root_dir; // 子目录

let tabbar = setting.tabbar; // http与https网站的接口处理

let getApiUrl = function (act, clt, mdu) {
    mdu = mdu || 'api';
    clt = clt || 'Api';
    let url = '';

    if (domain) {
        let domainArr = domain.split('/');

        if (domainArr[domainArr.length - 1].length == 0) {
            domain = domain.slice(0, -1);
        }
    }

    if (root_dir) {
        let rootDirArr = root_dir.split('/');

        if (rootDirArr[rootDirArr.length - 1].length == 0) {
            root_dir = root_dir.slice(0, -1);
        }

        if (rootDirArr[0].length != 0) {
            root_dir = root_dir = '/' + root_dir;
        }
    }

    if (true || domain.indexOf('https://') != -1) {
        // https网站直接请求站点api
        url = `${domain}${root_dir}/index.php?m=${mdu}&c=v1.${clt}&a=${act}&_ajax=1`;
    } else {
        // http网站经过官方https接口进行中转
        url = `https://service.eyysz.cn/index.php?m=api&c=MiniproButt&a=diyminiprocms&domain=${domain}&root_dir=${root_dir}&mdu=${mdu}&clt=${clt}&act=${act}&tpl=${tpl}&_ajax=1`;
    }

    return url;
};

let config = {
    // 底部导航菜单
    tabbar,
    // 小程序appid
    appId,
    // 开源小程序API接口密钥
    apikey,
    // 首页api地址
    apiIndexUrl: getApiUrl('index'),
    // 文档列表api地址
    apiListUrl: getApiUrl('archivesList'),
    // 文档详情页api地址
    apiViewUrl: getApiUrl('archivesView'),
    // 会员中心
    apiUsersdetailUrl: getApiUrl('users_detail'),
    // 用户登录
    apiUsersloginUrl: getApiUrl('users_login'),
    // 收藏/取消收藏
    apiGetCollectUrl: getApiUrl('get_collect', 'Users'),
    // 我的收藏列表
    apiGetCollectListUrl: getApiUrl('get_collect_list', 'Users'),
    // 上传头像
    userUploadHeadPicUrl: getApiUrl('upload_head_pic'),
    // 保存用户信息
    userSaveUserInfoUrl: getApiUrl('save_user_info', 'Users')
};
module.exports = config;
