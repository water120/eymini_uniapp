<template>
    <block>
        <!-- 搜索框 -->
        <view class="index-search-box">
            <navigator class="index-search" hover-class="none" openType="navigate" url="../search/index">
                <view class="index-cont-search t-c">
                    <icon class="text-center" color="rgba(0,0,0,0.4)" size="15" type="search"></icon>
                    <text>站内搜索</text>
                </view>
            </navigator>
        </view>

        <!-- 文档列表 start -->
        <view v-if="archivesList.data">
            <view class="block-newsList">
                <view class="news-item" @tap.stop.prevent="jumpView" :data-field="item" v-for="(item, index) in archivesList.data" :key="index">
                    <view class="news-item-l">
                        <image :src="item.litpic"></image>
                    </view>

                    <view class="news-item-r">
                        <view class="title">{{ item.title }}</view>
                        <view class="desc">{{ item.seo_description }}</view>
                        <view class="time">
                            <text class="iconfont icon-shijian"></text>
                            <text>{{ item.add_time }}</text>
                        </view>
                    </view>
                </view>
            </view>
            <view v-if="no_more" class="no-more f-30">亲, 没有更多了</view>
            <!-- 无数据提供的页面 -->
            <view v-if="!isLoading && !archivesList.data.length">
                <view class="ey-notcont">
                    <text class="iconfont icon-empty"></text>
                    <text class="cont">亲，没有符合数据</text>
                </view>
            </view>
        </view>
        <!-- 文档列表 end -->

        <!-- 这里是底部 -->
        <tabbar type="default"></tabbar>
    </block>
</template>

<script>
import tabbar from '@/components/tabbar/index';
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2020-1-1
 */
const app = getApp();

const func = require('../../utils/func.js');

export default {
    components: {
        tabbar
    },
    data() {
        return {
            keywords: '',
            // 搜索关键词
            archivesList: [],
            // 文档列表
            typeid: 0,
            // 当前的分类id (0则代表全部)
            no_more: false,
            // 没有更多数据
            isLoading: true,
            // 是否正在加载中
            page: 1 // 当前页码
        };
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this;

        if (options.scene) {
            //这里为线上操作
            // scene 需要使用 decodeURIComponent 才能获取到生成二维码时传入的 scene
            let scene = decodeURIComponent(options.scene);
            let typeid = func.getQueryVariable('typeid', scene);

            if (!typeid) {
                that.typeid = that.typeid;
            } else {
                that.typeid = typeid;
            }

            let keywords = func.getQueryVariable('keywords', scene);

            if (!keywords) {
                that.keywords = that.keywords;
            } else {
                that.keywords = keywords;
            }
        } else {
            //这里为开发环境
            if (options.typeid !== 'undefined') {
                if (!options.typeid) {
                    that.typeid = that.typeid;
                } else {
                    that.typeid = options.typeid;
                }
            }

            if (options.keywords !== 'undefined') {
                if (!options.keywords) {
                    that.keywords = that.keywords;
                } else {
                    that.keywords = options.keywords;
                }
            }
        }

        that.setData({
            typeid: that.typeid,
            keywords: that.keywords
        });
        that.getPageData(); // 获取页面数据
    },
    /**
     * 生命周期回调—监听页面显示
     */
    onShow() {},
    /**
     * 下拉刷新
     */
    onPullDownRefresh: function () {
        let that = this;
        that.page = 1;
        that.setData({
            page: that.page
        });
        that.getPageData(); // 获取页面数据

        uni.stopPullDownRefresh(); // 停止下拉刷新
    },
    /**
     * 分享当前页面
     */
    onShareAppMessage() {
        let _this = this; // 构建页面参数

        let params = app.globalData.getShareUrlParams({
            typeid: _this.typeid
        });
        return {
            title: `搜索词：` + _this.keywords,
            path: '/pages/search/list?' + params
        };
    },
    // 分享到朋友圈
    onShareTimeline() {
        let _this = this;

        return {
            title: `搜索词：` + _this.keywords
        };
    },
    methods: {
        /**
         * 获取页面数据
         */
        getPageData() {
            let _this = this;

            _this.getArchivesList(); // 获取文档列表
        },

        /**
         * 获取文档列表
         */
        getArchivesList(isPage, page) {
            let _this = this;

            let typeid = _this.typeid;
            let keywords = _this.keywords;
            app.globalData._requestApi(
                _this,
                app.globalData.config.apiListUrl,
                {
                    apiList: `ekey=1&keywords=${keywords}&typeid=${typeid}&page=${page}` // 文档列表分页标签list，列表页只存在一个apiList标签
                },
                function (res) {
                    let resList = res.data.apiList[1];
                    let // list文档列表分页数据
                        dataList = _this.archivesList; // 每次下拉分页之后的所有文档列表
                    // 特别说明：中括号[1]的数字必须与api标签的参数ekey=1值对应，否则数据对不上。
                    if (isPage == true) {
                        _this.setData({
                            'archivesList.data': dataList.data.concat(resList.data),
                            isLoading: false
                        });
                    } else {
                        // 设置导航标题
                        uni.setNavigationBarTitle({
                            title: `搜索词：` + keywords || '搜索列表'
                        });

                        _this.setData({
                            archivesList: resList,
                            isLoading: false
                        });
                    }
                }
            );
        },

        /**
         * 跳转详情页
         */
        jumpView(e) {
            func.jumpView(e);
        },

        /**
         * 下拉到底加载数据
         */
        bindDownLoad() {
            let _this = this; // 已经是最后一页

            if (_this.page >= _this.archivesList.last_page) {
                _this.setData({
                    no_more: true
                });

                return false;
            } // 加载下一页列表

            _this.getArchivesList(true, ++_this.page);
        }
    }
};
</script>
<style>
/**
 * 易优CMS
 * ============================================================================
 * 版权所有 2016-2028 海南赞赞网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.eyoucms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小虎哥 <1105415366@qq.com>
 * Date: 2020-1-1
 */

page {
    background-color: #f4f4f4;
    padding-bottom: 100rpx;
}

/* 搜索框 */

.index-cont-search {
    width: 100%;
    font-size: 28rpx;
    position: relative;
    background: #f7f7f7;
}

.index-cont-search icon {
    position: absolute;
    left: 50%;
    margin-left: -70rpx;
    top: 50%;
    margin-top: -15rpx;
}

.index-cont-search text {
    margin-left: 72rpx;
}

.index-search-box {
    background: #fff;
    padding: 13rpx 13rpx 18rpx 13rpx;
}

.index-search {
    border-bottom: 0;
    background: #fff;
    border-radius: 50rpx;
    overflow: hidden;
    position: relative;
    font-size: 28rpx;
    color: #999;
    box-sizing: border-box;
    height: 64rpx;
    line-height: 64rpx;
}

/*搜索列表*/
.block-newsList {
    display: flex;
    flex-wrap: wrap;
    background-color: #fff;
    padding: 0 30rpx;
    border-top: 1px solid #eee;
}

.block-newsList .news-item {
    display: flex;
    border-bottom: 1px solid #eee;
    padding: 30rpx 0;
}

.block-newsList .news-item:last-child {
    border-bottom: 0;
}

.block-newsList .news-item .news-item-l {
    width: 220rpx;
    margin-right: 20rpx;
}

.block-newsList .news-item .news-item-l image {
    width: 220rpx;
    height: 180rpx;
}

.block-newsList .news-item .news-item-r {
    width: 440rpx;
}

.block-newsList .news-item .news-item-r .title {
    font-size: 34rpx;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    color: #333;
}

.block-newsList .news-item .news-item-r .desc {
    margin-top: 16rpx;
    font-size: 24rpx;
    color: #999;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
    overflow: hidden;
}

.block-newsList .news-item .news-item-r .time {
    margin-top: 16rpx;
    font-size: 24rpx;
    color: #999;
}

.block-newsList .news-item .news-item-r .time .iconfont {
    font-size: 24rpx;
    margin-right: 10rpx;
}
</style>
